﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public Text respawnText;
    public Text controlsText;
    public Slider healthBar;
    public Text healthText;
    public Slider xpBar;
    public Text xpText;
    public Text levelText;
    public Text damageText;
    public Text armorText;

    private SpawnPlayer playerSpawnScript;

    private Stats playerStats;

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

	// Use this for initialization
	void Start () {
        init();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            if (!controlsText.enabled)
            {
                controlsText.enabled = true;
            }
            else
            {
                controlsText.enabled = false;
            }
        }
        // ha a játékos feltámadás alatt van, megjelenítjük a szöveget
        if (playerSpawnScript != null)
        {
            if (playerSpawnScript.isPlayerRespawning())
            {
                respawnText.enabled = true;
            }
        }

        if (respawnText.enabled)
        {
            if (playerSpawnScript.respawnTimer <= 0f)
            {
                respawnText.enabled = false;
            }
            else
            {
                respawnText.text = Mathf.Floor(playerSpawnScript.respawnTimer) + "s a feltámadásig!";
            }
        }

        healthBar.maxValue = playerStats.maxHealth;
        healthBar.value = playerStats.currentHealth;
        healthText.text = "HP : " + playerStats.currentHealth + "/" + playerStats.maxHealth;
        xpBar.minValue = playerStats.xpToLevel[playerStats.level - 1];
        xpBar.maxValue = playerStats.xpToLevel[playerStats.level];
        xpBar.value = playerStats.xp;
        xpText.text = "XP : " + playerStats.xp + "/" + playerStats.xpToLevel[playerStats.level];
        levelText.text = "LVL : " + playerStats.level;
        damageText.text = (playerStats.damage + playerStats.weaponDamage).ToString();
        armorText.text = playerStats.armor.ToString();
    }

    public void init()
    {
        GameObject currentMap = GameObject.FindGameObjectWithTag("Map");
        playerSpawnScript = currentMap.GetComponent<SpawnPlayer>();

        GameObject player = GameObject.FindGameObjectWithTag("Player");
        playerStats = player.GetComponent<Stats>();

        healthBar.minValue = 0;
    }
}
