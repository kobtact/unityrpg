﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnUI : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GameObject ui = GameObject.Find("UI");
        if(ui == null )
        {
            Debug.Log("Új UI spawnolt!");
            ui = Instantiate(Resources.Load("Prefabs/UI")) as GameObject;
            ui.name = "UI";
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
