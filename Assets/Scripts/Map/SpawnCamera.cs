﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCamera : MonoBehaviour {

	// Use this for initialization
	void Awake () {
        GameObject camera = GameObject.Find("Main Camera");

        if(camera == null)
        {
            camera = Instantiate(Resources.Load("Prefabs/Main Camera")) as GameObject;
            camera.name = "Main Camera";
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
