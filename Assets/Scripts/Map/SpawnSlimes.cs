﻿using System.Collections;
using System.Collections.Generic;
using Tiled2Unity;
using UnityEngine;

public class SpawnSlimes : MonoBehaviour {

    public GameObject objectToSpawn;

    // az esélye, hogy mezőnként spawnoljon-e egy slime
    public int chanceToSpawn;
    // maximum mennyit spawnoljon
    public int maxSpawn;
    // mennyivel beljebb kezdődjön a spawnolás
    public int spawnBorder;
    // max mennyi tile-nyi szélességben legyen spawn
    public int spawnWidthLimit;
    // max mennyi tile-nyi magasságban legyen spawn
    public int spawnHeightLimit;

    private TiledMap mapStats;

	// Use this for initialization
	void Start () {
        if(chanceToSpawn < 0)
        {
            chanceToSpawn = 0;
        }
        if(chanceToSpawn > 100)
        {
            chanceToSpawn = 100;
        }

        mapStats = GetComponent<TiledMap>();

        if(mapStats != null) { 
        
            if (spawnBorder < 0 || spawnBorder > mapStats.NumTilesHigh || spawnBorder > mapStats.NumTilesWide)
            {
                spawnBorder = 0;
            }

            // a tile-ok közepére szeretnénk spawnolni
            float middlePosition = spawnBorder + 0.5f;
            float widthLimit = mapStats.NumTilesWide - middlePosition - spawnWidthLimit;
            float heightLimit = mapStats.NumTilesHigh - middlePosition - spawnHeightLimit;

            for (float i = middlePosition; maxSpawn > 0 && i < widthLimit; i+=3)
            {
                for (float j = -middlePosition; maxSpawn > 0 && j > -heightLimit; j-=3)
                {
                    float random = Random.value;
                    if((float)chanceToSpawn / 100 >= random)
                    {
                        maxSpawn--;
                        Instantiate(objectToSpawn, new Vector3(i, j, 0f), Quaternion.identity);
                    }
                }
            }   
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
