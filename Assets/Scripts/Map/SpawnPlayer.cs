﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SpawnPlayer : MonoBehaviour {

    public float spawnX;
    public float spawnY;

    // respawnolási idő (egyelőre csak játékosra)
    // TODO megjeleníteni GUI-n, hogy mennyi idő van még hátra
    private float respawnDuration = 5f;
    public float respawnTimer;
    private bool isRespawning = false;
    private bool respawn321SoundPlayed = false;
    private bool respawnSoundPlayed = false;

    private AudioManager audioManager = null;
    private UIManager uiManager = null;

    // Use this for initialization
    void Start() {

        // audiomanager megkeresése
        GameObject amObject = GameObject.Find("AudioManager");
        if (amObject != null)
        {
            audioManager = amObject.GetComponent<AudioManager>();
        }
        FindUiManager();

        respawnSoundPlayed = false;
        respawn321SoundPlayed = false;
        GameObject player = GameObject.Find("Player");
        // ha létezett a játékos, csak beállítjuk a pozícióját
        if (player != null)
        {
            player.transform.position = new Vector3(spawnX, spawnY, 0f);
        }
        // egyébként létrehozunk egy újat
        else
        {
            Debug.Log("Új játékos spawnolt!");
            player = Instantiate(Resources.Load("Prefabs/Player"), new Vector3(spawnX, spawnY, 0f), Quaternion.identity) as GameObject;

            player.name = "Player";
            uiManager.init();
        }

        // majd beállítjuk a kamerát a játékosra
        GameObject camera = GameObject.Find("Main Camera");

        CameraControl cameraScript = null;
        if (camera != null)
        {
            cameraScript = camera.GetComponent<CameraControl>();
        }

        if (cameraScript != null)
        {
            cameraScript.target = player.transform;
        }
    }

    // Update is called once per frame
    void Update()
    {
        // respawnolás, ha lejárt a beállított idő, újra meghívjuk az Start-et
        // és így spawnoljuk újra a játékost
        if (isRespawning)
        {
            if (respawnTimer <= 0f)
            {
                isRespawning = false;
                if (!respawnSoundPlayed)
                {
                    audioManager.PlaySound("Respawn");
                    respawnSoundPlayed = true;
                }

                Start();
            }
            else if (!respawn321SoundPlayed && respawnTimer <= 4f)
            {
                audioManager.PlaySound("Respawn321");
                respawn321SoundPlayed = true;
            }
            respawnTimer -= Time.deltaTime;
        }
    }

    private void FindUiManager()
    {
        GameObject go = GameObject.Find("UI");
        if (go != null)
        {
            uiManager = go.GetComponent<UIManager>();
        }
    }

    // a játékos Stats szkriptje hívja meg
    private void playerRespawning()
    {
        respawnTimer = respawnDuration;
        isRespawning = true;
    }

    public bool isPlayerRespawning()
    {
        return isRespawning;
    }
}
