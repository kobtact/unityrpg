﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Load : MonoBehaviour {

    private bool playerNear = false;
    public string mapName;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (playerNear && Input.GetKeyDown(KeyCode.Return))
        {
            SceneManager.LoadScene(mapName);
        }
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            playerNear = true;
        }
        else
        {
            playerNear = false;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        playerNear = false;
    }
}
