﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnAudioManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GameObject amObject = GameObject.Find("AudioManager");
        if (amObject == null)
        {
            Debug.Log("Új Audio Manager spawnolt!");
            amObject = Instantiate(Resources.Load("Prefabs/AudioManager") as GameObject);
            amObject.name = "AudioManager";
        }
        amObject.SendMessage("Start");
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
