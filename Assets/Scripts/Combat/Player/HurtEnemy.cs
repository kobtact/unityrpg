﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtEnemy : MonoBehaviour {

    // sérülés részecske effekt
    public GameObject bloodsplatter;

    // játékos és ellenfél statisztikái
    private Stats playerStats, enemyStats;

    // hang effektekhez
    private AudioManager audioManager;

    // Use this for initialization
    void Start () {
        playerStats = GameObject.Find("Player").GetComponent<Stats>();
        GameObject amObject = GameObject.Find("AudioManager");
        if (amObject != null)
        {
            audioManager = amObject.GetComponent<AudioManager>();
        }
    }
	
	// Update is called once per frame
	void Update () {

	}

    // ha egy ellenfél vagy egy elpusztítható 
    // GameObject colliderével érintkezik, sebezze neg
    private void OnTriggerEnter2D(Collider2D collision)
    {
        enemyStats = collision.gameObject.GetComponent<Stats>();
        if (collision.gameObject.tag == "Enemy" || collision.gameObject.tag == "Destructible")
        {
            Debug.Log(playerStats.name + " " + enemyStats.TakeDamage(playerStats) + " sebzést okozott " + enemyStats.name + " - nek");
        }
        if (collision.gameObject.tag == "Enemy")
        {
            Instantiate(bloodsplatter, transform.position, transform.rotation);
            audioManager.PlaySound("EnemyHurt");
        }
    }
}
