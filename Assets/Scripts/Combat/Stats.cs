﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// játékos vagy NPC statisztikái ( név, élet, sebzés, páncél, stb...)
public class Stats : MonoBehaviour {

    private new Collider2D collider;

    private new SpriteRenderer renderer;

    // halhatatlansági idő sérülés után, és számláló
    public float invincibilityDuration;
    private float invincibilityTimer;

    // játékos vagy NPC neve
    public new string name;

    // maximum életpontja
    public int maxHealth;

    // jelenlegi életpontjai
    public int currentHealth;

    // páncélja
    public int armor;

    // sebzése
    public int damage;

    // fegyver sebzése
    public int weaponDamage = 0;

    // szint
    public int level;

    // tapasztalati pont
    public int xp;

    // szintlépési határok
    public int[] xpToLevel;

    // játékos objektum
    private GameObject player;

    // hang effektekhez
    private AudioManager audioManager = null;

    // szintetlépés részecske effekt
    public GameObject levelUpParticle;

    // Use this for initialization
    void Start () {

        currentHealth = maxHealth;
        collider = GetComponent<Collider2D>();
        renderer = GetComponent<SpriteRenderer>();
        if (tag != "Destructible")
        {
            UpdateWeaponDamage();
        }

        if (tag == "Player")
        {
            DontDestroyOnLoad(gameObject);
        }
        player = GameObject.Find("Player");

        FindAudioManager();
    }
	
	// Update is called once per frame
	void Update () {
        Stats playerStats = null;
        if (player != null)
        {
            playerStats = player.GetComponent<Stats>();
        }
        
        if (currentHealth <= 0)
        {
            if (tag != "Player")
            {
                if(tag != "Destructible")
                {
                    audioManager.PlaySound("Death");
                }
                gameObject.SetActive(false);
                // hozzáadjuk az xp-jét a játékoséhoz
                if (player == null)
                {
                    player = GameObject.Find("Player");
                }
                playerStats = player.GetComponent<Stats>();
                playerStats.xp += xp;
                Debug.Log("+" + xp + " XP!!!");
            }
            else
            {
                GameObject currentMap = GameObject.FindGameObjectWithTag("Map");
                currentMap.SendMessage("playerRespawning");

                Destroy(gameObject);
            }
        }

        Color currentColor = renderer.color;
        if (invincibilityTimer > 0f)
        {
            invincibilityTimer -= Time.deltaTime;
            // játékos esetében "villogó" hatás a sérülés után
            if(tag == "Player")
            {
                if (invincibilityTimer > invincibilityDuration * 2 / 3 || invincibilityTimer <= invincibilityDuration / 3)
                {
                    renderer.color = new Color(currentColor.r, currentColor.g, currentColor.b, 0f);
                }
                else if (invincibilityTimer < invincibilityDuration * 2 / 3 && invincibilityTimer > invincibilityDuration / 3)
                {
                    renderer.color = new Color(currentColor.r, currentColor.g, currentColor.b, 1f);
                }
            }
        }
        else
        {
            renderer.color = new Color(currentColor.r, currentColor.g, currentColor.b, 1f);
        }

        // frame végén megnézzük kell-e a játékost szintet lépetni
        if (playerStats != null)
        {
             CheckLevelUp(playerStats);
        }
	}

    // sebződés
    public int TakeDamage(Stats enemyStats)
    {
        int damage;
        if (invincibilityTimer <= 0)
        {
            damage = enemyStats.damage + enemyStats.weaponDamage - armor;
            if (damage < 0)
            {
                damage = 0;
            }
            if (damage > 0)
            {
                currentHealth -= damage;
                // collider kikapcsolása egy időre a sérülés után ( a problémák elkerülése végett )
                invincibilityTimer = invincibilityDuration;
            }
        }
        else
        {
            damage = 0;
        }
        
        return damage;
    }

    // fegyver sebzésének frissítése (pl.: ha felvettünk egy új fegyvert)
    public void UpdateWeaponDamage()
    {
        if(transform.childCount > 0)
        {
            GameObject weaponHolder = gameObject.transform.Find("Weapon").gameObject;
            if (weaponHolder != null)
            {
                GameObject weapon = weaponHolder.transform.GetChild(0).gameObject;
                if (weapon != null)
                {
                    weaponDamage = weapon.GetComponent<WeaponStats>().damage;
                }
            }
        }
    }

    // szintlépés ellenőrzése, ellenfél legyőzése után
    private void CheckLevelUp(Stats playerStats)
    {
        // ha nincs több tömb elem a következő szinthez, akkor elérte a maximális szintet
        if(playerStats.level + 1 <= playerStats.xpToLevel.Length)
        {
            // ha a következő szintlépés határt elérte, mnöveljük a statisztikákat (ilyen nagyon nem interaktív módon)
            if (playerStats.xp >= playerStats.xpToLevel[playerStats.level])
            {
                playerStats.level++;
                GameObject go = Instantiate(levelUpParticle);

                go.transform.parent = player.transform;
                Vector3 playerPos = player.transform.position;
                go.transform.position = new Vector3(playerPos.x, playerPos.y - 0.5f, playerPos.z);
                FindAudioManager();
                audioManager.PlaySound("LevelUp");

                playerStats.maxHealth += 2;
                playerStats.currentHealth = playerStats.maxHealth;
                playerStats.damage += 2;
                playerStats.armor++;
            }
        }
    }

    private void FindAudioManager()
    {
        if (audioManager == null)
        {
            GameObject amObject = GameObject.Find("AudioManager");
            if (amObject != null)
            {
                audioManager = amObject.GetComponent<AudioManager>();
            }
        }
    }
}
