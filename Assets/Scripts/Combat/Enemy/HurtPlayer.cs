﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtPlayer : MonoBehaviour {

    // sebzés
    private Stats enemyStats, otherStats;

    // hang effektekhez
    private AudioManager audioManager;

    // Use this for initialization
    void Start () {
        enemyStats = GetComponent<Stats>();
        GameObject amObject = GameObject.Find("AudioManager");
        if (amObject != null)
        {
            audioManager = amObject.GetComponent<AudioManager>();
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player" || collision.gameObject.tag == "Destructible")
        {
            otherStats = collision.gameObject.GetComponent<Stats>();
            Debug.Log(enemyStats.name + " " + otherStats.TakeDamage(enemyStats) + " sebzést okozott " + otherStats.name + " - nek");
            audioManager.PlaySound("PlayerHurt");
        }
    }
}
