﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeMovement : MonoBehaviour {

    private float moveSpeed = 6;
    private Vector2 moveDirection = Vector2.zero;

    private float moveTimeCounter = 0f;

    private new Rigidbody2D rigidbody;

	// Use this for initialization
	void Start () {
        rigidbody = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
        if (moveTimeCounter > 0f)
        {
            moveTimeCounter -= Time.deltaTime;
        }
        else
        {
            moveTimeCounter = Random.Range(5f, 10f);
            moveSpeed = Random.Range(10f, 30f);
            float randomX = Random.Range(-1f, 1f);
            float randomY = Random.Range(-1f, 1f);
            //Debug.Log("slime_x : " + randomX + " slime_y : " + randomY);
            moveDirection = new Vector2(randomX, randomY);
            rigidbody.velocity = new Vector2(moveDirection.x * moveSpeed / 2, moveDirection.y * moveSpeed / 2);
        }
        
    }
}
