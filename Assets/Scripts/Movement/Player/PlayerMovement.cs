﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public float characterSpeed;

    private Animator animator;
    private new Rigidbody2D rigidbody;

    // hang effektekhez
    private AudioManager audioManager;

    private float axisLimit = 0.1f;

    private bool isPlayerMoving;
    private bool isPlayerAttacking;
    private bool attackSoundPlayed;

    // támadási animáció közben nincs mozgás
    // ehhez számláló
    public float attackTime;
    private float attackTimeCounter;

    private Vector2 lastMove;

    private void Awake()
    {
        lastMove = new Vector2(0f, 0f);
        animator = GetComponent<Animator>();
        rigidbody = GetComponent<Rigidbody2D>();
        GameObject amObject = GameObject.Find("AudioManager");
        if (amObject != null)
        {
            audioManager = amObject.GetComponent<AudioManager>();
        }
        isPlayerAttacking = false;
        attackSoundPlayed = false;
    }

    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
        float horizontalDirection = Input.GetAxisRaw("Horizontal");
        float verticalDirection = Input.GetAxisRaw("Vertical");

        isPlayerMoving = false;

        // ha nem támad a játékos, mozoghat
        if (!isPlayerAttacking)
        {
            // vízszintes mozgás
            if (horizontalDirection < -axisLimit || horizontalDirection > axisLimit)
            {
                isPlayerMoving = true;
                lastMove = new Vector2(horizontalDirection, 0f);
                rigidbody.velocity = new Vector2(horizontalDirection * characterSpeed, rigidbody.velocity.y);
            }
            else
            {
                rigidbody.velocity = new Vector2(0f, rigidbody.velocity.y);
            }

            // függőleges mozgás
            if (verticalDirection < -axisLimit || verticalDirection > axisLimit)
            {
                isPlayerMoving = true;
                lastMove = new Vector2(0f, verticalDirection);
                rigidbody.velocity = new Vector2(rigidbody.velocity.x, verticalDirection * characterSpeed);
            }
            else
            {
                rigidbody.velocity = new Vector2(rigidbody.velocity.x, 0f);
            }

            // a játékos támadott
            if (Input.GetButtonDown("Fire1"))
            {
                isPlayerAttacking = true;
                attackSoundPlayed = false;
                attackTimeCounter = attackTime;
                rigidbody.velocity = Vector2.zero;
            }
        }
        else
        {
            if (attackTimeCounter > 0)
            {
                attackTimeCounter -= Time.deltaTime;
            }
            else if (attackTimeCounter <= 0)
            {
                isPlayerAttacking = false;
            }

            if(!attackSoundPlayed && attackTimeCounter < 0.5f)
            {
                audioManager.PlaySound("Attack");
                attackSoundPlayed = true;
            }
        }

        animator.SetBool("isPlayerMoving", isPlayerMoving);
        animator.SetBool("isPlayerAttacking", isPlayerAttacking);

        animator.SetFloat("moveX", horizontalDirection);
        animator.SetFloat("moveY", verticalDirection);
        animator.SetFloat("lastMoveX", lastMove.x);
        animator.SetFloat("lastMoveY", lastMove.y);
    }
}
